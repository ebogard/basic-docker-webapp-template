import os
import json
from flask import Flask
from pymongo import MongoClient

class Encoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return super(Encoder, self).default(obj) 

app = Flask(__name__)
app.json_encoder = Encoder
client = MongoClient(os.environ['MONGO_CONNECT'])
default_db = client['admin']


@app.route('/status')
def get_status():
    col = default_db['system.version']
    return col.find_one({'_id':'authSchema'})
    
    
